﻿//Сначала, увидев в сообщении со ссылкой на тестовое задание фразу "реализация на уровне промышленного качества", я стал реализовывать
//интерфейсы и классы с рассчетом на dependency injection. Но затем, решив, что это всё же overengineering для такой задачи, сделал все максимально просто


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Spelling
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Пожалуйста, передайте абсолютный путь к файлу как аргумент. Нажмите любую клавишу для завершения.");
                Console.ReadLine();
                return;
            }
            string fileLocation = args[0];

            ICollection<string> caseLines = ReadFromFile(fileLocation);
            if (caseLines == null)
            {
                return;
            }

            List<string> convertedCases = new List<string> { Environment.NewLine };
            int counter = 1;
            foreach (var caseLine in caseLines)
            {
                convertedCases.Add(ConvertCase(caseLine, counter));
                counter++;
            }

            WriteToFile(fileLocation, convertedCases);
        }

        static ICollection<string> ReadFromFile(string fileLocation)
        {
            string[] fileLines;
            List<string> caseLines = new List<string>();
            try
            {
                fileLines = File.ReadAllLines(fileLocation);
            }
            catch (Exception ex)
            {
                if (ex is PathTooLongException || ex is DirectoryNotFoundException || ex is FileNotFoundException)
                {
                    Console.WriteLine("Пожалуйста, проверьте правильность пути к файлу. Нажмите любую клавишу для завершения.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Не удалось прочитать файл. Нажмите любую клавишу для завершения.");
                    Console.ReadLine();
                }
                return null;
            }

            if (fileLines.Length < 2)
            {
                Console.WriteLine("Содержимое файла не соответствует условиям задачи. Нажмите любую клавишу для завершения.");
                Console.ReadLine();
                return null;
            }

            int numberOfCases;
            if (!int.TryParse(fileLines[0], out numberOfCases) || numberOfCases < 0)
            {
                Console.WriteLine("В первой строке файла должно содержаться неотрицательное число. Нажмите любую клавишу для завершения.");
                Console.ReadLine();
                return null;
            }

            if (numberOfCases >= fileLines.Length)
            {
                Console.WriteLine("Число, содержащееся в первой строке, превышает количество последующих строк. Нажмите любую клавишу для завершения.");
                Console.ReadLine();
                return null;
            }

            for (int i = 1; i <= numberOfCases; i++)
            {
                if (!fileLines[i].All(c => (c >= 'a' && c <= 'z') || c == ' '))
                {
                    Console.WriteLine("Во второй и последующих строках файла должны содержаться только строчные буквы латинского алфавита, или пробелы. Нажмите любую клавишу для завершения.");
                    Console.ReadLine();
                    return null;
                }
                caseLines.Add(fileLines[i]);
            }
            return caseLines;
        }

        static string ConvertCase(string caseLine, int counter)
        {

            string convertedCase = string.Format("Case #{0}: ", counter);
            char prevDigit = '$'; //инициализируется символом, который никогда не встретится во входных данных.

            foreach (var letter in caseLine)
            {
                string curDigits = "";
                switch (letter)
                {
                    case 'a': curDigits = "2"; break;
                    case 'b': curDigits = "22"; break;
                    case 'c': curDigits = "222"; break;
                    case 'd': curDigits = "3"; break;
                    case 'e': curDigits = "33"; break;
                    case 'f': curDigits = "333"; break;
                    case 'g': curDigits = "4"; break;
                    case 'h': curDigits = "44"; break;
                    case 'i': curDigits = "444"; break;
                    case 'j': curDigits = "5"; break;
                    case 'k': curDigits = "55"; break;
                    case 'l': curDigits = "555"; break;
                    case 'm': curDigits = "6"; break;
                    case 'n': curDigits = "66"; break;
                    case 'o': curDigits = "666"; break;
                    case 'p': curDigits = "7"; break;
                    case 'q': curDigits = "77"; break;
                    case 'r': curDigits = "777"; break;
                    case 's': curDigits = "7777"; break;
                    case 't': curDigits = "8"; break;
                    case 'u': curDigits = "88"; break;
                    case 'v': curDigits = "888"; break;
                    case 'w': curDigits = "9"; break;
                    case 'x': curDigits = "99"; break;
                    case 'y': curDigits = "999"; break;
                    case 'z': curDigits = "9999"; break;
                    case ' ': curDigits = "0"; break;
                    default:
                        Console.WriteLine("Во второй и последующих строках файла должны содержаться только строчные буквы латинского алфавита, или пробелы. Нажмите любую клавишу для завершения.");
                        Console.ReadLine();
                        return null;
                }

                if (prevDigit == curDigits[curDigits.Length - 1])
                {
                    convertedCase += ' ';
                }
                convertedCase += curDigits;
                prevDigit = curDigits[curDigits.Length - 1];
            }
            return convertedCase;
        }

        static void WriteToFile(string fileLocation, IEnumerable<string> caseLines)
        {
            try
            {
                File.AppendAllLines(fileLocation, caseLines);
            }
            catch (Exception ex)
            {
                if (ex is PathTooLongException || ex is DirectoryNotFoundException)
                {
                    Console.WriteLine("Пожалуйста, проверьте правильность пути к файлу. Нажмите любую клавишу для завершения.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Не удалось записать информацию в файл. Нажмите любую клавишу для завершения.");
                    Console.ReadLine();
                }
            }
        }
    }
}

